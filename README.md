# Madym binaries
This project is used to host installer packages for the [Madym C++ toolkit](https://gitlab.com/manchester_qbi/manchester_qbi_public/madym_cxx/-/wikis/home).

Binary installers for Mac (OS 10 + OS 11), Windows and Ubuntu are built offline from release branches of [Madym source](https://gitlab.com/manchester_qbi/manchester_qbi_public/madym_cxx), and pushed directly to the master branch in this repository, together with some simple HTML files indexing links to download the installers.

This triggers GitLab's CI/CD pipeline to publish the download links on GitLab pages. A link to these pages in maintained on the main [Madym C++ wiki](https://gitlab.com/manchester_qbi/manchester_qbi_public/madym_cxx/-/wikis/prebuilt_binaries).

In the future it may be nice to more fully automate this in a complete CI/CD pipeline, however the current semi-automated approach suits our current needs.
